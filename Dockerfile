FROM node:15.3.0-alpine as builder

ENV PORT 3000
EXPOSE 3000

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

WORKDIR /app
RUN npm i -g @angular/cli
COPY package.json package-lock.json /app/
RUN node --version
RUN npm -v
RUN ng version
RUN npm install
COPY src /app/src

CMD [ "ng", "build" ]
