export class PictureValidator {

    public static validPictureExtension = ['jpeg', 'png', 'jpg'];

    static extension(file): any {
      if (file.pristine) {
        return null;
      }

      file.markAsTouched();

      // Extension
      let extension = null;
      if (file) {
        const split = file.value.split('.');
        extension = split[1];

        if (extension === 'jpeg') {
          extension = 'jpg';
        }

        if (PictureValidator.validPictureExtension.indexOf(extension.toLowerCase()) === -1) {
          return {
            invalidExtension: true,
          };
        }
      }
      return null;
    }
  }
