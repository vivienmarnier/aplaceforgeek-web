export class Publication{

    id: number;
    title: string;
    message: string;
    author: string;
    game: string;
    date: string;

    constructor(title: string,message: string, game: string){
        this.title = title;
        this.message = message;
        this.game = game;
    }
}
