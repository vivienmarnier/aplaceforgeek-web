import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from '@env/environment';
import { User } from '@models/user.model';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AccountService {

  public userSubject: BehaviorSubject<User>;
  private registerUrl: string = environment.baseUrl + '/api/user-registration/registration';
  private loginUrl: string = environment.baseUrl + '/api/login_check';
  private loggedUser: User;

  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService, private jwtHelperService: JwtHelperService) {
    this.userSubject = new BehaviorSubject(null);
  }

  public registerUser(user: any): Observable<any>{
    const headers = new HttpHeaders({ 'Content-Type': 'application/json'});
      return this.http.post<any>(`${this.registerUrl}`,JSON.stringify(user), { headers});
  }

  /**
   * Log in user from Form
   *
   * @param user
   */
  public login(user: any): Observable<any>{

    const headers = new HttpHeaders({ 'Content-Type': 'application/json'});
      return this.http.post<any>(`${this.loginUrl}`,JSON.stringify(user), { headers}).pipe(map(result => {
        this.storeSessionToken(result.token);
        this.setupUser(result.token);
        this.toastr.success('You are now connected !','Welcome',{positionClass: 'toast-bottom-right'});
      }));
  }

  /**
   * Log out user
   */
  public logout(){
    if(localStorage.getItem('token')){
      this.loggedUser = null;
      this.userSubject.next(null);
      localStorage.removeItem('token');
      this.toastr.success('You have been logged out !','Logout',{positionClass: 'toast-bottom-right'});
      this.router.navigateByUrl('/');
    }
  }

  public reconnect(){
    const token = localStorage.getItem('token');
    if(token && !this.jwtHelperService.isTokenExpired(token)){
      this.setupUser(token);
    }

    return false;
  }

  /**
   * Check if logged user has admin role
   */
  public isUserAdmin(): boolean{
    if(this.isLoggedIn() && this.loggedUser.roles.find(role => role === 'ROLE_ADMIN')){
      return true;
    }
    return false;
  }

  public isLoggedIn(){
    const token = localStorage.getItem('token');
    if(this.loggedUser && !this.jwtHelperService.isTokenExpired(token)){
      return true;
    }
    return false;
  }

  public getUser(){
    return this.loggedUser;
  }

  private setupUser(token: string) {
    const userInfos = this.jwtHelperService.decodeToken(token);
    this.loggedUser = new User();
    this.loggedUser.id = userInfos.id;
    this.loggedUser.email = userInfos.email;
    this.loggedUser.roles = userInfos.roles;
    this.userSubject.next(this.loggedUser);
  }

  private storeSessionToken(token: any){
      localStorage.setItem('token', token);
  }
}
