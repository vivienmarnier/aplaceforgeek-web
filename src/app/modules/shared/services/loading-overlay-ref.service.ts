import { OverlayRef } from '@angular/cdk/overlay';

export class LoadingOverlayRef {
    constructor(private overlayRef: OverlayRef) { }

    close(): void {
      this.overlayRef.dispose();
    }
  }
