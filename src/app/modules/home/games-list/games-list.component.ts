import { Component, OnInit } from '@angular/core';
import { Game } from '@models/game.model';
import { GameService } from '@services/game.service';

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.css']
})
export class GamesListComponent implements OnInit {

  public games: Array<Game> = [];

  constructor(private gameService: GameService) { }

  ngOnInit(): void {
    this.loadGames();
  }

  public subscribe(game: Game){
    this.gameService.subscribe(game).subscribe(result => {
        console.log(result);
    });
  }

  /**
   * Load games list
   */
  private loadGames(){
    this.gameService.getPaginatorList(0,1,'').subscribe(result => {
      this.games = result.games;
    });
  }
}
